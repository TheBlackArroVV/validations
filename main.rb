require_relative 'validation'

class Main
  class TestError < StandardError; end

  include Validation

  attr_reader :name

  validate :name, format: /[abc]/
  validate :name, presence: true
  validate :name, type: String

  def initialize(name)
    @name = name
  end

  def self.test
    {'a' => true, 'z' => false,'' => false, nil => false, 1 => false}.each do |param, expectation|
      raise TestError, "Test failed with #{param}, expected #{expectation}" unless self.new(param).valid? == expectation
    end
  end
end
