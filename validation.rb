module Validation
  def self.included(base)
    base.extend ClassMethods
    base.send :include, InstanceMethods
  end

  module ClassMethods
    ValidationRule = Struct.new(:attribute, :type, :rule)

    def validate(attribute, rule)
      validations << ValidationRule.new(attribute, rule.keys.first, rule.values.first)
    end

    def validations
      @validations ||= []
    end
  end

  module InstanceMethods
    class ValidationError < ArgumentError; end

    def validate!
      validations.each do |validation|
        validation_method = "#{validation.type}_validation"
        public_send(validation_method, public_send(validation.attribute), validation.rule)
      end

      true
    end

    def validations
      self.class.validations
    end

    def valid?
      validate!
    rescue ValidationError
      false
    end

    def presence_validation(variable, mode)
      return true unless mode

      raise ValidationError, 'Can`t be blank' if variable == '' || variable.nil?
      true
    end

    def format_validation(variable, format)
      raise ValidationError, 'Not in REG EXP' if variable !~ format

      true
    end

    def type_validation(variable, type)
      raise ValidationError, 'Wrong type' unless variable.is_a?(type)

      true
    end
  end
end
